from django.shortcuts import render

from rest_framework import generics
from .serializers import SchoolClassSerializer
from .models import SchoolClass

class CreateView(generics.ListCreateAPIView):
    """This class defines the create behavior of our REST API"""
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer

    def perform_create(self, serializer):
        """Save the post date when creating a new school class"""
        serializer.save()

class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer