from rest_framework import serializers
from .models import SchoolClass

class SchoolClassSerializer(serializers.ModelSerializer):
    """Serializer to map the model instance into JSON format"""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = SchoolClass
        fields = ('id', 'name', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')
