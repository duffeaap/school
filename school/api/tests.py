from django.test import TestCase
from .models import SchoolClass
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse

class ModelTestCase(TestCase):
    """This class defines the test suite for the SchoolPresence model"""

    def setUp(self):
        """Define the test client and other test variables"""
        self.schoolclass_name = "Write world class code"
        self.schoolclass = SchoolClass(name=self.schoolclass_name)

    def test_model_can_create_schoolclass(self):
        old_count = SchoolClass.objects.count()
        self.schoolclass.save()
        new_count = SchoolClass.objects.count()
        self.assertNotEqual(old_count, new_count)

class ViewTestCase(TestCase):
    """Test suite for the api views"""

    def setUp(self):
        """Define the test client and the other test variables"""
        self.client = APIClient()
        self.schoolclass_data = {'name': 'Onderbouw'}
        self.response = self.client.post(
            reverse('create'), self.schoolclass_data, format="json"
        )

    def test_api_can_create_schoolclass(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_schoolclass(self):
        schoolclass = SchoolClass.objects.get()
        response = self.client.get(
            reverse('details', kwargs={'pk': schoolclass.id}), format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, schoolclass)

    def test_api_can_update_schoolclass(self):
        schoolclass = SchoolClass.objects.get()
        change_schoolclass = {'name': 'New Class'}
        res = self.client.put(
            reverse('details', kwargs={'pk': schoolclass.id}), change_schoolclass, format='json'
        )

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_schoolclass(self):
        schoolclass = SchoolClass.objects.get()
        response = self.client.delete(
            reverse('details', kwargs={'pk': schoolclass.id}), format='json', follow=True
        )

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)


